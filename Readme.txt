*********************   SAF Version 1.62   *******************
Updates in SAF version 1.62:
1. Updated the LCS rotation angle calculation for Y by vector, Z by point, Y by point.
2. Modified the code for creating the perametric circular cross section.
3. Observed a mismatch of the numbering of curve members created in Robot and Parsed SAF file. Observed this issue when testing 2 SAF files and Resolved the issue.
4. Updated the closing window issue(Closing of the Robot application when clicking on close button). 
5. Updated the status of the each event accuretley. 

*********************   SAF Version 1.61   *******************
Updates in SAF version 1.61:
1. Added mapping of Surface Member Regions.
2. Updated the system line positioning(Left, Right, Tope left, Bottom left, Top right and Bottom right) of structural curve members.
3. Updated the Rotation angle calulcation.
4. Added Behaviour in analysis of structural curve members.
5. Added Structural surfaces creation with variable thickness(along the xy plan)
6. Added defult cross sections for unavailable sections in Robot. 
7. Updated the log file information to understand the exact missing data in SAF input file.
8. Corrected the mistake of creating the bars with different numbers and not matching with SAF file.

*********************   SAF Version 1.6   *******************
Updates in SAF version 1.6:
1. Ignoring the capitalization of column header strings.
2. Updated the LCS orientation and application of the rotation angle(theta) to all the structural members.
3. Implemented the circular opening using 'Circle and point'. 
4. Creates the cross sections according to dimensions given for 'General' type members instead of default cross sections(default cross section is considered in previous version)
5. Assigning the rib offsets properly.
6. Applying the Eccentricities and System line position properly to all the structural curve members.
7. Circular diameter was doubled prevously and now creating the cross sections with correct diameter
8. Height and width dimensions are considered in the same way for beams and columns. It was assumed that height is considered to be the larger dimension for column and width is considered to be the larger dimensions for beam in the previous versions.
9. Handling of the LCS orientation for panels.

*********************   SAF Version 1.5   *******************
Updates in SAF version 1.5:
1. Added structural curve edges(Density lines) in STAAD panels.
2. STAAD function is updated to map the sections of each country properly. Updated the method for mapping the sections.
3. Added the information about structural curvemember varying is not applied in STAAD and Robot
4. Added the information about system line is not applied for structural curve members for STAAD application.
5. Logfile captures the structural curve member/surface/rib if there is any error, creates other geometrical entities except the one with issues.
6. logging infromation for Parsing SAF file,curve members,surface,curve edges,ribs is updated to give more information to the user.

*********************   SAF Version 1.4   *******************
Updates in SAF version 1.4:
1. Added the SAF version check and added alert message to make sure that user used version beyond 1.0.5.
2. Added the logging information of parsing file.
3. Removed the unnecessary/duplicate data and added the required data to the Robot function.
4. Added the E_tolerance and check with Youngs of modulus from SAF, E from from SAF less than tolerance then default material properties will be created.
5. Added the Python logging instead of text file creation for generating the log files.


